package com.bootcamp.cicd.ContinousIntegrationDeploymentSample.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApplicationController {

    class BookStore {
        private String name = "CI/CD Book Store";
        private String location = "Pune";
        private String contact = "999999999";

        public String getName() {
            return this.name;
        }

        public String getLocation() {
            return location;
        }

        public String getContact() {
            return contact;
        }
    }

    @GetMapping(value = "/application")
    public BookStore helloWorld() {
        return new BookStore();
    }

}
