package com.bootcamp.cicd.ContinousIntegrationDeploymentSample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContinousIntegrationDeploymentSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContinousIntegrationDeploymentSampleApplication.class, args);
	}

}
