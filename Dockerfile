FROM adoptopenjdk/openjdk11
ADD /build/libs/ContinousIntegrationDeploymentSample-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]